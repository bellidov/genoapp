﻿using GenoApp.Functions;
using GenoApp.GeneticOperators;
using System;
using System.Linq;
using System.Windows.Forms;

namespace GenoApp
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            buttonRun.Enabled = false;
        }

        private IFunction function;
        private double[] minArgs, maxArgs; // minimalnie i maximalnie dopustimie znachenia argumentov
        private GA genetic;

        private void buttonFunction_Click(object sender, EventArgs e)
        {
            Intervals intervals = new Intervals(this, textFunction.Text); //Form
            intervals.ShowDialog(this);
            buttonRun.Enabled = true;
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            setDataToGen();
            double[] result = genetic.FindMinArg(function);
            textBox2.Text = string.Join(", ", result.Select(p=>p.ToString()).ToArray());
        }

        // set parameters for genetic algoritm run
        private void setDataToGen()
        {
            setArgs();

            //set genetic operators
            DateTime startTime = DateTime.Now;
            String fileName;

            IGeneticOperator localAdaptation;

            if (checkBox1.Checked)
            {
                localAdaptation = new LocalAdaptation(minArgs, maxArgs);
                fileName = pathToSave + "\\GA_Hybrid_" + startTime.ToString("hh_mm_ss") + ".txt";
            }
            else
            {
                localAdaptation = new NothingToDo();
                fileName = pathToSave + "\\GA_Clasic_" + startTime.ToString("hh_mm_ss") + ".txt";
            }

            genetic = new GA(minArgs, maxArgs, Convert.ToInt32(generations.Text), Convert.ToInt32(populationSize.Text));
            genetic.setOperators(
                    new InitRandomPopulation(minArgs, maxArgs, Convert.ToInt32(populationSize.Text)),
                    new WriterLog(fileName, startTime),
                    new ArithmeticCrossover(Convert.ToInt32(crossingRate.Text)),
                    new Mutation(Convert.ToInt32(mutationRate.Text), minArgs, maxArgs),
                    localAdaptation,
                    new Selector(Convert.ToInt32(populationSize.Text)));  
        }

        //set functions and min/max args in form Intervals
        public void setArgs(Intervals intervals)
        {
            minArgs = intervals.getMinArgs();
            maxArgs = intervals.getMaxArgs();
            function = intervals.getFunction();

        }

        public void setArgs()
        {
            int dim = Convert.ToInt32(textBox1.Text);
            minArgs = getRasInterval(dim, false);
            maxArgs = getRasInterval(dim, true);
            //   function = new Rastrigin(dim);
            function = new Schwefel(dim);
        }

        public double[] getRasInterval(int dim, bool max)
        {
            double[] interval = new double[dim];
            for(int i = 0; i < dim; i++)
            {
                //  interval[i] = max ? 5.12 : -5.12;
                interval[i] = max ? 500 : -500;
            }
            return interval;
        }

        FolderBrowserDialog savef = new FolderBrowserDialog();
        String pathToSave;
        private void button1_Click(object sender, EventArgs e)
        {
            if (savef.ShowDialog() == DialogResult.OK)
            {
                pathToSave = savef.SelectedPath;
            }
        }
    }
}
