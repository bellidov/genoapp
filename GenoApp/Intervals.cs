﻿using GenoApp.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenoApp
{
    public partial class Intervals : Form
    {
        public Intervals(Form form, String textFormula)
        {
            InitializeComponent();
            formula = new Formula(textFormula);
            this.form = form;
            panel1.AutoScroll = true;
        }

        private Form form;
        private IFormula formula;
        private List<TextBox> minArgsText = new List<TextBox>();
        private List<TextBox> maxArgsText = new List<TextBox>();
        private string[] nameArgs;

        private void Intervals_Load(object sender, EventArgs e)
        {
            nameArgs = formula.getArgs().ToArray();
            for(int i = 0; i < nameArgs.Length; i++)
            {
                putLabelArg(i);
                putArgText(i, 50);
                putArgText(i, 125);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            ((Main)form).setArgs(this);
            this.Close();
        }

        public double[] getMinArgs()
        {
            double[] minArgs = new double[nameArgs.Length];
            for (int i = 0; i < nameArgs.Length; i++)
            {
                minArgs[i] = Convert.ToDouble(minArgsText[i].Text);
            }
            return minArgs;
        }

        public double[] getMaxArgs()
        {
            double[] maxArgs = new double[nameArgs.Length];
            for (int i = 0; i < nameArgs.Length; i++)
            {
                maxArgs[i] = Convert.ToDouble(maxArgsText[i].Text);
            }
            return maxArgs;
        }

        public IFunction getFunction()
        {
            IFunction function = new Function(formula);
            return function;
        }

        //Cancel
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // set Graphic Elements
        private void putLabelArg(int i)
        {
            Label label = new Label();
            label.Text = nameArgs[i];
            label.Location = new Point(20, 10 + 25 * i);
            label.Size = new System.Drawing.Size(50, 20);
            panel1.Controls.Add(label);
        }

        private void putArgText(int i, int left)
        {
            TextBox arg = new TextBox();
            arg.Text = left == 50 ? "-10" : "10";
            arg.Size = new System.Drawing.Size(50, 20);
            arg.Location = new Point(left + 50, 10 + 25 * i);
            panel1.Controls.Add(arg);

            if (left == 50)
            {
                minArgsText.Add(arg);
            }
            else
            {
                maxArgsText.Add(arg);
            }
        }
    }
}
