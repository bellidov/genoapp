﻿using GenoApp.Functions;
using GenoApp.Genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class Mutation : IGeneticOperator
    {
        private int mutationRate;
        double[] a, b;


        public Mutation(int mutationRate, double[] a, double[] b)
        {
            this.a = a;
            this.b = b;
            this.mutationRate = mutationRate;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            int L = population[0].getGenotype().Length; // dlina genotype
            int N = population.Count;
            int Fdim = f.getDim();

            Random r = new Random();
            int count = 0;
            int max = (int)(mutationRate * N / 100);

            while (count < max)
            {
                int ip = r.Next(0, N); // index dlia populiatsii
                int ig = r.Next(0, L); // index dlia genotype
                double value = r.NextDouble() * (b[ig] - a[ig]) + a[ig]; // sluchainoe razreshennoe znachenie dlia gena

                double[] genotemp = new double[Fdim];
                for (int i = 0; i < Fdim; i++)
                {
                    genotemp[i] = population[ip].getGenotype()[i]; // sozdayu mutirovannij gen
                }
                genotemp[ig] = value; // sobstvenno, mutatsia

                population.Add(new Genotype(genotemp, f));

                count++;
            }
            return population;
        }
    }
}
