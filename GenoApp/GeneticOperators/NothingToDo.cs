﻿using GenoApp.Functions;
using GenoApp.Genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class NothingToDo : IGeneticOperator
    {
        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            return population;
        }
    }
}
