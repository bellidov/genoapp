﻿using GenoApp.Drawers;
using GenoApp.Functions;
using GenoApp.Genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class DrawerPopulation : IGeneticOperator
    {
        private IIDrawer drawer;
        private double min, max;
        private int index;
        public DrawerPopulation(IIDrawer drawer, double min, double max, int index)
        {
            this.drawer = drawer;
            this.min = min;
            this.max = max;
            this.index = index;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            drawer.clear();
            population.Sort(new Comp());
            ((IDrawableFunction)f).Draw(population[0].getGenotype(), min, max, index, drawer);
            //risuyu tochku minimuma
            drawer.drawPoint(population[0].getGenotype()[index], population[0].getRang());

            foreach (var v in population)
            {
                drawer.drawPoint(v.getGenotype()[index], v.getRang());
            }

            //   Thread.Sleep(1000);
            return population;
        }
    }
}
