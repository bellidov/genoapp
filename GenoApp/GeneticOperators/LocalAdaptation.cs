﻿using GenoApp.Functions;
using GenoApp.Genotypes;
using GenoApp.Optimizators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class LocalAdaptation : IGeneticOperator
    {
        private IOptimizator finder;
        private int dim = 0;
        double[] a, b;

        public LocalAdaptation(double[] a, double[] b)
        {
            this.a = a;
            this.b = b;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            List<IGenotype> popu = new List<IGenotype>(); // to chto budet na vyxode

            int count = 0;
            if (dim == 0)
            {
                dim = f.getDim();
            }

            // cycle proxodit po kazhdoi osobi v populiatsii
            while (count < population.Count)
            {
                double[] argMin = (double[])population[count].getGenotype().Clone(); // delayu kopiu tekushego massiva
                IGenotype genoMin; // budet dobavliatsa v populiatsiu popu

                // cycl proxodit po kazhdomu elementu tekushego genotypa
                for (int i = 0; i < dim; i++)
                {
                    genoMin = new Genotype(argMin, f);

                    double[] argTemp = (double[])argMin.Clone(); // vremenno, dlia sravnenia
                    finder = new Strongin(a[i], b[i], argTemp, i, 1); // po ocheredi uluchaetsa kazhdiy element genotypa
                    argTemp[i] = finder.argMin(f);
                    IGenotype genoTemp = new Genotype(argTemp, f);

                    // uslovie ostanova
                    if (genoTemp.getRang() <= genoMin.getRang())
                    {
                        //esli bylo ulushenie, to soxraniayu ego
                        argMin = (double[])argTemp.Clone();
                    }
                    else
                    {
                        //esli net ulushenia, to vyxozhu is tsikla chtoby pereiti k sleduyushei osobi
                        break;
                    }

                }

                genoMin = new Genotype(argMin, f);
                popu.Add(genoMin);
                count++;
            }
            //fin de ciclo

            return popu;
        }
    }
}
