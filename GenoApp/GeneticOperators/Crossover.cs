﻿using GenoApp.Functions;
using GenoApp.Genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class Crossover : IGeneticOperator
    {
        private int crossingRate;

        public Crossover()
        {

        }

        public Crossover(int crossingRate)
        {
            this.crossingRate = crossingRate;
        }

        public virtual List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            int L = population[0].getGenotype().Length;
            int N = population.Count;
            int Fdim = f.getDim();
            int cross;

            Random r = new Random();
            cross = r.Next(1, L); // sluchainaya tochka dlia skreshivania

            double[] buff = new double[L];
            for (int k = 0; k < N - 1; k = k + 2) // beru po 2 osobi i skreshivayu ix
            {
                double[] child1 = new double[Fdim];
                double[] child2 = new double[Fdim];

                for (int i = 0; i < cross; i++)
                {
                    child1[i] = population[k].getGenotype()[i];
                    child2[i] = population[k + 1].getGenotype()[i];
                }
                for (int i = cross; i < L; i++)
                {
                    child1[i] = population[k + 1].getGenotype()[i];
                    child2[i] = population[k].getGenotype()[i];
                }

                population.Add(new Genotype(child1, f));
                population.Add(new Genotype(child2, f));
            }
            return population;
        }
    }
}
