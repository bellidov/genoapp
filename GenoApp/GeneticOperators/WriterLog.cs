﻿using GenoApp.Functions;
using GenoApp.Genotypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class WriterLog : IGeneticOperator
    {
        private DateTime current, start;
        private string fileName;


        public WriterLog(String fileName, DateTime start)
        {

            this.start = start;
            this.fileName = fileName;
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            //    fileName = "pios.txt";
            using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Default))
            {
                double bestFitness = getBestFitness(population);
                double averageFitness = getFitnessMedium(population);
                current = DateTime.Now;
                double interval = current.Subtract(start).TotalMilliseconds;
                writer.WriteLine("{0} {1} {2} {3}", bestFitness, averageFitness, interval, DateTime.Now.ToString("HH:mm:ss:fff"));
            }
            return population;
        }

        // vychisliaet srednuyu prisposoblennost dlia kazhdoi populiatsii
        private double getFitnessMedium(IEnumerable<IGenotype> population)
        {
            return population.Average(genotype => genotype.getRang());
        }

        private double getBestFitness(IEnumerable<IGenotype> population)
        {
            return population.Min(genotype => genotype.getRang());
        }
    }
}
