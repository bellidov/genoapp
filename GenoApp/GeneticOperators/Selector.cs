﻿using GenoApp.Functions;
using GenoApp.Genotypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.GeneticOperators
{
    class Selector : IGeneticOperator
    {
        private int N; // nachalniy zadanniy razmer populiatsii

        public Selector(int N)
        {
            this.N = N;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            population.Sort(new Comp());
            population.RemoveRange(N, population.Count - N);
            return population;
        }
    }
}
