﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenoApp.Genotypes;
using GenoApp.Functions;

namespace GenoApp.GeneticOperators
{
    class InitRandomPopulation : IGeneticOperator
    {
        private int N;
        private double[] a, b;

        public InitRandomPopulation(double[] a, double[] b, int N)
        {
            this.N = N;
            this.a = (double[])a.Clone();
            this.b = (double[])b.Clone();
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            if (population.Count != 0)
            {
                return population;
            }

            Random r = new Random();
            int Fdim = f.getDim();
            double[] genotemp = new double[Fdim];

            for (int i = 0; i < N; i++)
            {
                int j = 0;
                while (j < Fdim)
                {
                    genotemp[j] = r.NextDouble() * (b[j] - a[j]) + a[j];  // beru sluchainie DOPUSTIMIE parametri argumenta
                    j++;
                }
                population.Add(new Genotype(genotemp, f));  // dobavliayu v populiatsiu
            }

            return population;
        }
    }


}
