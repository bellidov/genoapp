﻿namespace GenoApp.Drawers
{
    public interface IDrawer
    {
        void drawLine(double x1, double y1, double x2, double y2);
        void drawPoint(double x, double y);
        void clear();
    }
}
