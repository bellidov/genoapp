﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenoApp.Drawers
{
    class SimpleDrawer : IIDrawer
    {
        private Graphics g;
        private Pen p;

        public SimpleDrawer(Graphics g, Pen p)
        {
            this.g = g;
            this.p = p;
        }

        public void drawLine(double x1, double y1, double x2, double y2)
        {
            g.DrawLine(p, convertX(x1), convertY(y1), convertX(x2), convertY(y2));
        }

        public void drawPoint(double x, double y)
        {
            SolidBrush b = new SolidBrush(Color.Blue);
            g.FillEllipse(b, convertX(x), convertY(y), 5, 5);
        }


        private int convertX(double x)
        {
            int temp = (int)(600 * ((x - Xmin) * (1 - mx) + mx * (Xmax - x)) / (Xmax - Xmin));
            return temp;
        }

        private int convertY(double y)
        {
            int temp = (int)(400 * ((Ymax - y) * (1 - my) + (y - Ymin) * my) / (Ymax - Ymin));
            return temp;
        }

        double Ymin, Ymax, Xmin, Xmax; //koordinaty dlia grafiki
        double mx = 0.01, my = 0.05; // margin dlia grafiki

        public void setBorders(double[] borders)
        {
            this.Xmin = borders[0];
            this.Xmax = borders[1];
            this.Ymin = borders[2];
            this.Ymax = borders[3];
        }


        public void clear()
        {
            if (g != null)
                g.Clear(Color.White);
        }
    }
}
