﻿namespace GenoApp
{
    partial class Intervals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.ArgCol = new System.Windows.Forms.Label();
            this.MinCol = new System.Windows.Forms.Label();
            this.MaxCol = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(12, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 329);
            this.panel1.TabIndex = 0;
            // 
            // ArgCol
            // 
            this.ArgCol.AutoSize = true;
            this.ArgCol.Location = new System.Drawing.Point(25, 9);
            this.ArgCol.Name = "ArgCol";
            this.ArgCol.Size = new System.Drawing.Size(30, 17);
            this.ArgCol.TabIndex = 1;
            this.ArgCol.Text = "Arg";
            // 
            // MinCol
            // 
            this.MinCol.AutoSize = true;
            this.MinCol.Location = new System.Drawing.Point(112, 9);
            this.MinCol.Name = "MinCol";
            this.MinCol.Size = new System.Drawing.Size(30, 17);
            this.MinCol.TabIndex = 2;
            this.MinCol.Text = "Min";
            // 
            // MaxCol
            // 
            this.MaxCol.AutoSize = true;
            this.MaxCol.Location = new System.Drawing.Point(191, 9);
            this.MaxCol.Name = "MaxCol";
            this.MaxCol.Size = new System.Drawing.Size(33, 17);
            this.MaxCol.TabIndex = 3;
            this.MaxCol.Text = "Max";
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOK.Location = new System.Drawing.Point(12, 375);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(105, 44);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(158, 377);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(105, 44);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "CANCEL";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // Intervals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 431);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.MaxCol);
            this.Controls.Add(this.MinCol);
            this.Controls.Add(this.ArgCol);
            this.Controls.Add(this.panel1);
            this.Name = "Intervals";
            this.Text = "Intervals";
            this.Load += new System.EventHandler(this.Intervals_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ArgCol;
        private System.Windows.Forms.Label MinCol;
        private System.Windows.Forms.Label MaxCol;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;


    }
}