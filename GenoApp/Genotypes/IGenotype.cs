﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Genotypes
{
    interface IGenotype
    {
        double getRang();
        double[] getGenotype();
    }
}
