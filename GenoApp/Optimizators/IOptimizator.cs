﻿using GenoApp.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Optimizators
{
    interface IOptimizator
    {
        double argMin(IFunction f);
    }
}
