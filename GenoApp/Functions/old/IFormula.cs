﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Functions
{
    public interface IFormula
    {
        double eval(double[] x);
        int getDim();
        List<string> getArgs();
    }
}
