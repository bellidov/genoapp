﻿using GenoApp.Drawers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Functions
{
    class Function : IFunction
    {
        IFormula fm;
        public Function(IFormula fm)
        {
            this.fm = fm;
        }

        public double getValue(double[] x)
        {
            return fm.eval(x);
       //     return (x[0] * x[1] + x[5] - x[2] + x[3] * x[4] * x[6] - Math.Pow(x[7], 2) - Math.Pow(x[8], 3) + x[9] * x[10]) * Math.Cos(x[0] + 2 * x[1] - x[2] - 50 * x[3] + 2 * x[4] - 25 * x[5] + 3 * x[6] - x[7] + 7 * x[8] + 2 * x[9] - 5 * x[10] + 45);
        }

        public int getDim()
        {
        //    return 11; // 
            return fm.getDim();
        }

        List<double> P = null;

        public void Draw(double[] bestGenotype, double min, double max, int index, IDrawer d)
        {

            // razvivayu os' na shagi i soxraniau tochki v P
            int N = 500; // kolichestvo tochek dlia otrisovki

            P = new List<double>();
            double shag = (max - min) / N;
            for (int i = 0; i < N; i++)
                P.Add(min + i * shag);

            double[] _a = (double[])bestGenotype.Clone();
            double[] _b = (double[])_a.Clone();

            //risuyu grafik funtksii
            for (int i = 0; i < N - 1; i++)
            {
                _a[index] = P[i];
                _b[index] = P[i + 1];

                double test = this.getValue(_a);

                // kostyl - predotvrashaet otrisovku nedopustimix tochek
                double funValue = this.getValue(_b);
                if (!Double.IsNaN(test) && !Double.IsNaN(funValue))
                {
                    double xc1 = _a[index];
                    double yc1 = test;
                    double xc2 = _b[index];
                    double yc2 = funValue;

                    d.drawLine(xc1, yc1, xc2, yc2);
                }
            }
        }
    }
}
