﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenoApp.Drawers;

namespace GenoApp.Functions
{
    public class Rastrigin : IFunction
    {
        private int dim = 0;

        public Rastrigin(int dim)
        {
            this.dim = dim;
        }

        public int getDim()
        {
            return dim;
        }

        public double getValue(double[] x)
        {
            if(x.Length != dim) throw new Exception("Razmer ne sovpadaet");

            double value = 10*dim;

            for(int i = 0; i < dim; i++)
            {
                value += Math.Pow(x[i], 2) - 10 * Math.Cos(2 * Math.PI * x[i]);
            }

            return value;
        }
    }
}
