﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Functions
{
    public class Schwefel : IFunction
    {
        private int dim;
        public Schwefel(int dim)
        {
            this.dim = dim;
        }

        public int getDim()
        {
            return dim;
        }

        public double getValue(double[] x)
        {
            if (x.Length != dim) throw new Exception("razmeri ne sovpadayut");

            double result = 418.9829*dim;

            for(int i = 0; i < dim; i++)
            {
                result -= x[i] * Math.Sin(Math.Sqrt(Math.Abs(x[i])));
            }

            return result;
        }
    }
}
