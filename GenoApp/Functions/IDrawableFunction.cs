﻿using GenoApp.Drawers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Functions
{
    public interface IDrawableFunction
    {
        void Draw(double[] constants, double min, double max, int index, IDrawer d);
    }
}
