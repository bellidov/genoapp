﻿using GenoApp.Drawers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenoApp.Functions
{
    public interface IFunction
    {
        double getValue(double[] x);
        int getDim();
    }
}
