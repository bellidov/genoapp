﻿using GenoApp.Functions;
using GenoApp.GeneticOperators;
using GenoApp.Genotypes;
using System.Collections.Generic;
using System.Linq;

namespace GenoApp
{
    class GA
    {
        private int N;//razmer populiatsii
        private List<IGenotype> population; //soderzhit vozmozhnie reshenia
        private int generations;    
        private List<IGeneticOperator> operators;

        public GA(double[] a, double[] b, int generations, int popuSize)
        {
            population = new List<IGenotype>();
            this.generations = generations; // chislo pokolenij dlia uslovia ostanova
            this.N = popuSize;
        }

        //ustanavlivaet geneticheskie operatori
        public void setOperators(params IGeneticOperator[] oper)
        {
            operators = new List<IGeneticOperator>();
            foreach (var v in oper)
            {
                operators.Add(v);
            }
        }

        //sam geneticheskiy algoritm soderzhitsa v etom metode
        public double[] FindMinArg(IFunction f)
        {
            int Fdim = f.getDim();
            int count = 0;
            int M = 1; // razmer odnogo genotipa = 1 t.k. bez kodirovania poka
            // dlina osobi (genotypa):
            int L = M * Fdim;   // in this case L genotype (osob) =  dim of function because we are working without coder

            population = operators[0].getPopulation(population, f);  // set init population
            double fitnessValue = population[0].getRang();

            while (count < generations) // poka schetchik ne dostignet zadannoe chislo
            {
                // vypolniautsa vse geneticheskie operatori podriad, krome initPopulation
                for (int i = 1; i < operators.Count; i++ )
                {
                    population = operators[i].getPopulation(population, f);
                }

                ////   uslovie ostanova
                double buff = population.Min(genotype => genotype.getRang());
                if (buff >= fitnessValue)
                {
                    count++;
                }
                else
                {
                    count = 0;
                    fitnessValue = buff;
                }
            }
            return population[0].getGenotype(); // iz-za sortirovki, samaya udachnaya osob vsegda v nachale spiska
        }

        // vozvrashaet ves nador vozmozhnix reshenij soderzhashixsia v populiatsii
        public List<IGenotype> getPopulation()
        {
            return this.population;
        }
    }
}
