﻿
using GenoApp.Functions;
using System;

namespace Terminal
{
    class Program
    {
        static void Main(string[] args)
        {
            IFunction function = new Schwefel(2);//new Rastrigin(3);
            double v = 420.9687;
            try
            {
                Console.WriteLine(function.getValue(new double[] { v, v }));
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }
    }
}
